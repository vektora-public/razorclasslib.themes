
$.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    columnDefs: [{
        orderable: false,
        width: '100px'
    }],
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filtre:</span> _INPUT_',
        searchPlaceholder: 'Filtrelemek i�in yaz�n.',
        lengthMenu: '<span>G�ster:</span> _MENU_',
        paginate: { 'first': '�lk', 'last': 'Son', 'next': '&rarr;', 'previous': '&larr;' },
        sDecimal: ",",
        sEmptyTable: "Tabloda herhangi bir veri mevcut de�il",
        sInfo: "_TOTAL_ kay�ttan _START_ - _END_ aras�ndaki kay�tlar g�steriliyor",
        sInfoEmpty: "Kay�t yok",
        sInfoFiltered: "(_MAX_ kay�t i�erisinden bulunan)",
        sInfoPostFix: "",
        sInfoThousands: ".",
        sLengthMenu: "Sayfada _MENU_ kay�t g�ster",
        sLoadingRecords: "Y�kleniyor...",
        sProcessing: "��leniyor...",
        sSearch: "Ara:",
        sZeroRecords: "E�le�en kay�t bulunamad�",
        oPaginate: {
            "sFirst": "�lk",
            "sLast": "Son",
            "sNext": "Sonraki",
            "sPrevious": "�nceki"
        },
        oAria: {
            "sSortAscending": ": artan s�tun s�ralamas�n� aktifle�tir",
            "sSortDescending": ": azalan s�tun s�ralamas�n� aktifle�tir"
        },
        select: {
            "rows": {
                "_": "%d kay�t se�ildi",
                "0": "",
                "1": "1 kay�t se�ildi"
            }
        }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});