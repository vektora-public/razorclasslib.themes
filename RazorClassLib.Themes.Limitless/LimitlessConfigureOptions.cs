﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;

namespace RazorClassLib.Themes.Limitless
{
    public class LimitlessConfigureOptions : IPostConfigureOptions<StaticFileOptions>
    {
        private readonly IHostingEnvironment _environment;

        public LimitlessConfigureOptions(IHostingEnvironment environment) => _environment = environment;

        public void PostConfigure(string name, StaticFileOptions options)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (options == null) throw new ArgumentNullException(nameof(options));

            options.ContentTypeProvider = options.ContentTypeProvider ?? new FileExtensionContentTypeProvider();

            if (options.FileProvider == null && _environment.WebRootFileProvider == null)
                throw new InvalidOperationException("Missing File Provider.");

            options.FileProvider = options.FileProvider ?? _environment.WebRootFileProvider;

            ManifestEmbeddedFileProvider filesProvider = new ManifestEmbeddedFileProvider(GetType().Assembly, "wwwroot");
            options.FileProvider = new CompositeFileProvider(options.FileProvider, filesProvider);
        }
    }

    public static class LimitlessServiceCollectionExtensions
    {
        public static void AddLimitless(this IServiceCollection services)
        {
            services.ConfigureOptions(typeof(LimitlessConfigureOptions));
            services.Configure<RazorViewEngineOptions>(opt =>
            {
                opt.FileProviders.Add(new EmbeddedFileProvider(typeof(LimitlessConfigureOptions).Assembly, "RazorClassLib.Themes.Limitless"));
            });
        }
    }
}